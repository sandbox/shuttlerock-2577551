# MMG Shuttlerock

[Shuttlerock](https://www.shuttlerock.com/) is a social content platform used to integrate various social media platforms into a single resource.  These sources can include photos, products, and comments, all of which can be voted upon or shared.  The contest feature is particularly interesting, as it allows users to share their stories and experiences with others.  Content can optionally be operated.

For more information on Shuttlerock, see [their Help section](http://help.shuttlerock.com/).  For more information on the Shuttlerock JSON API used by this module, see the [Shuttlerock API documentation](http://developer.shuttlerock.com/) page.

## Table of Contents

