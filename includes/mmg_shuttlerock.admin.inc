<?php
/**
 * @file
 * Administrative code for the MMG Shuttlerock module.
 */

/**
 * Landing page for the administrative section of Shuttlerock for the site.
 * @return string
 */
function mmg_shuttlerock_admin_page() {
  return '';
}

/**
 * Settings form for the module.
 */
function mmg_shuttlerock_admin_form($form, &$form_state) {
  $form['mmg_shuttlerock_ie9_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable IE9 support'),
    '#description' => t('IE9 requires some extra markup to be added to a page with a Shuttlerock widget.'),
    '#default_value' => variable_get('mmg_shuttlerock_ie9_enabled', TRUE),
  );

  return system_settings_form($form);
}

/**
 * Page for configuring the default values used for Shuttlerock entities.
 */
function mmg_shuttlerock_defaults_page() {
  return drupal_get_form('mmg_shuttlerock_default_settings_form');
}

/**
 * Form for Shuttlerock Board Entity modifications.
 */
function shuttlerock_board_form($form, &$form_state, $entity = NULL) {
  $form = $form + _mmg_shuttlerock_form_options('board', $entity);

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => !empty($entity->sbid) ? t('Update board') : t('Save board'),
    ),
  );

  return $form;
}

/**
 * Submit handler for the Shuttlerock Board Entity modification form.
 */
function shuttlerock_board_form_submit($form, &$form_state) {
  $entity = entity_ui_form_submit_build_entity($form, $form_state);
  $entity->save();

  drupal_set_message(t('Shuttlerock Board %name has been saved.', array('%name' => $entity->board_id)));

  $form_state['redirect'] = 'admin/shuttlerock/board';
}

/**
 * Form for Shuttlerock Widget Entity modifications.
 */
function shuttlerock_widget_form($form, &$form_state, $entity = NULL) {
  $form = $form + _mmg_shuttlerock_form_options('widget', $entity);

  $form['base']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Administrative Name'),
    '#description' => t('A human-readable name to identify this widget in administration.'),
    '#required' => TRUE,
    '#default_value' => isset($entity->name) ? $entity->name : '',
    '#weight' => -10,
  );

  // Generate a list of available boards
  $boards = array();
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'shuttlerock_board')
    ->propertyOrderBy('board_id');

  $result = $query->execute();
  if (!empty($result['shuttlerock_board'])) {
      $boards = entity_load('shuttlerock_board', array_keys($result['shuttlerock_board']));
  }

  $form['base']['sbid'] = array(
    '#type' => 'select',
    '#title' => t('Board'),
    '#required' => TRUE,
    '#default_value' => isset($entity->sbid) ? $entity->sbid : 0,
    '#options' => array(),
    '#weight' => -9,
  );

  foreach ($boards as $board) {
    $form['base']['sbid']['#options'][$board->sbid] = $board->board_id;
  }

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => !empty($entity->sbid) ? t('Update widget') : t('Save widget'),
    ),
  );

  return $form;
}

/**
 * Submit handler for the Shuttlerock Widget Entity modification form.
 */
function shuttlerock_widget_form_submit($form, &$form_state) {
  $entity = entity_ui_form_submit_build_entity($form, $form_state);
  $entity->save();

  drupal_set_message(t('Shuttlerock Widget %name has been saved.', array('%name' => $entity->name)));

  $form_state['redirect'] = 'admin/shuttlerock/widget';
}

/**
 * Default Shuttlerock settings form.
 */
function mmg_shuttlerock_default_settings_form($form, &$form_state) {
  $form = _mmg_shuttlerock_default_options($form_state['input']);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
 * Submit handler for the mmg_shuttlerock_settings form.
 */
function mmg_shuttlerock_default_settings_form_submit($form, &$form_state) {
  $values = &$form_state['values'];

  variable_set('mmg_shuttlerock_default_settings', $values);
  drupal_set_message('Shuttlerock default settings have been saved');
}

/**
 * Helper function to determine the default value of a Social Hub setting.
 * @param $name string Name of the setting
 * @param $default mixed The default value of the setting
 * @param array $settings Array of settings to customize default values
 * @return mixed Returns the desired default value
 */
function _mmg_shuttlerock_widget_option_default($name, $default, $settings = array()) {
  return array_key_exists($name, $settings) ? $settings[$name] : $default;
}

/**
 * Integer validator for the Social Hub widget options.
 */
function _mmg_shuttlerock_validate_int($element, &$form_state, $form) {
  if (!empty($element['#value']) && !ctype_digit($element['#value'])) {
    form_error($element, t('This must be an integer.'));
  }
}

/**
 * Minimum value validator.
 */
function _mmg_shuttlerock_validate_minimum($element, &$form_state, $form) {
  if (strlen($element['#value']) > 0 && $element['#value'] < $element['#shuttlerock_minimum']) {
    form_error($element, t('This value cannot be less than @min.', array('@min' => $element['#shuttlerock_minimum'])));
  }
}

/**
 * Maximum value validator.
 */
function _mmg_shuttlerock_validate_maximum($element, &$form_state, $form) {
  if (strlen($element['#value']) > 0 && $element['#value'] > $element['#shuttlerock_maximum']) {
    form_error($element, t('This value cannot be greater than @max.', array('@max' => $element['#shuttlerock_maximum'])));
  }
}

/**
 * Build up the form options available based on the type of entity in use.
 * @param string $type The type of Shuttlerock entity: board or widget
 * @param object $entity An optional entity object, used for populating default values
 * @return array Returns a Form API-compliant array
 */
function _mmg_shuttlerock_form_options($type, $entity = NULL) {
  $settings = !is_null($entity) ? (array)$entity : array();

  return _mmg_shuttlerock_build_options($settings, $type);
}

/**
 * Generates the entity default settings form.
 * @param array $settings An array of values to use instead of Drupal variables
 * @return array Returns the form array, ready for use in a form
 */
function _mmg_shuttlerock_default_options($settings = array()) {
  if (empty($settings)) {
    $settings = variable_get('mmg_shuttlerock_default_settings', array());
  }

  return _mmg_shuttlerock_build_options($settings);
}

/**
 * Builds an options form.
 * @param array $settings An array of values used to populate defaults
 * @param string $type An optional type of entity to build the form for
 * @return array Returns a Form API-compliant array representing the desired options
 */
function _mmg_shuttlerock_build_options($settings, $type = NULL) {
  $form = array();
  $options = mmg_shuttlerock_entity_options();

  foreach ($options as $field => $data) {
    // If we're building a form for a specific type, skip those options that don't match
    if (!is_null($type) && $data['entity'] != $type) {
      continue;
    }

    // Grouping
    $group = $data['group'];
    if (empty($form[$group])) {
      $form[$group] = array(
        '#type' => 'fieldset',
        '#title' => t(ucfirst($group)),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
    }

    // Field generation
    $key = strtolower(preg_replace('!([a-z])([A-Z])!', '$1_$2', str_replace('_', '__', $field)));
    $form[$group][$key] = array(
      '#title' => $data['name'],
      '#description' => $data['description'],
      '#required' => !empty($data['required']),
      '#access' => !array_key_exists('access', $data) || !empty($data['access']),
      '#default_value' => _mmg_shuttlerock_widget_option_default($key, $data['default'], $settings),
    );

    // Type processing
    if (!empty($data['options'])) {
      // If 'options' exists, make this a select
      $form[$group][$key]['#type'] = 'select';
      $form[$group][$key]['#options'] = $data['options'];
    } elseif ($data['type'] == 'bool') {
      // Booleans are represented as checkboxes
      $form[$group][$key]['#type'] = 'checkbox';
    } else {
      // Everything else defaults to a textfield, for manual entry
      $form[$group][$key]['#type'] = 'textfield';
    }

    // Element validation
    if ($data['type'] == 'int') {
      // Integer validation
      $form[$group][$key]['#element_validate'] = array('_mmg_shuttlerock_validate_int');

      if (!empty($data['minimum'])) {
        $form[$group][$key]['#shuttlerock_minimum'] = $data['minimum'];
        $form[$group][$key]['#element_validate'][] = '_mmg_shuttlerock_validate_minimum';
      }

      if (!empty($data['maximum'])) {
        $form[$group][$key]['#shuttlerock_maximum'] = $data['maximum'];
        $form[$group][$key]['#element_validate'][] = '_mmg_shuttlerock_validate_maximum';
      }

      // Alter the description to indicate any range restrictions
      if (!empty($data['minimum']) && !empty($data['maximum'])) {
        $form[$group][$key]['#description'] .= t(' Must be a value from @min to @max.', array('@min' => $data['minimum'], '@max' => $data['maximum']));
      } elseif (!empty($data['maximum'])) {
        $form[$group][$key]['#description'] .= t(' The value cannot exceede @max.', array('@max' => $data['maximum']));
      } elseif (!empty($data['minimum'])) {
        $form[$group][$key]['#description'] .= t(' The value cannot be below @min.', array('@min' => $data['minimum']));
      }
    }
  }

  return $form;
}
