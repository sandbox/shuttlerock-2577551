<?php
/**
 * @file
 * Includes for in-code theme and UI components.
 */

/**
 * Returns HTML for a Shuttlerock Board Entity label.
 */
function theme_shuttlerock_ui_board_overview_item($variables) {
  $output = $variables['url'] ? l($variables['label'], $variables['url']['path'], $variables['url']['options']) : check_plain($variables['label']);
  return $output;
}

/**
 * Returns HTML for a Shuttlerock Widget Entity label.
 */
function theme_shuttlerock_ui_widget_overview_item($variables) {
  $output = $variables['url'] ? l($variables['label'], $variables['url']['path'], $variables['url']['options']) : check_plain($variables['label']);
  return $output;
}
