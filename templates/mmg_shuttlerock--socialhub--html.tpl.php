<?php
/**
 * @file
 * Embeds all of the markup needed for the Social Hub into the page.
 */

$settings = _mmg_shuttlerock_socialhub_js_settings($socialhubName, FALSE);

?>
<div data-socialhub-name="<?= $socialhubName ?>" ng-cloak ng-controller="SocialHubController" class="<?= implode(' ', $classes_array) ?>" ng-social-hub
     <?php if (!empty($settings)): ?>
       <?php foreach ($settings as $key => $value): ?>
         ng-social-hub-<?= strtolower(preg_replace('!([A-Z])!', '-$1', $key)) ?>="<?= htmlspecialchars($value) ?>"
       <?php endforeach; ?>
     <?php endif; ?>></div>