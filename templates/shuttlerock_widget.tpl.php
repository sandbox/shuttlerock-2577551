<?php
/**
 * @file
 * Takes care of rendering the Shuttlerock widget.
 */
?>
<div class="<?= $classes ?>"<?= $attributes ?>></div>
