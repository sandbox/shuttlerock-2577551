<?php
/**
 * @file
 * Markup necessary for IE9 support.
 */

$customer = $entity->customer;
$api_suffix = $entity->api_suffix;

?>
<!-- Start xdomain -->
<!--[if IE]>
<script type='text/javascript'>
  window.isIE = true;
</script>
<script type="text/javascript" src="//jpillora.com/xdomain/dist/xdomain.js"></script>
<script type='text/javascript'>
  if (typeof(xdomain) != 'undefined') {
    xdomain.slaves({
      "https://<?= $customer ?><?= $api_suffix ?>":"/xdomain/proxy"
    });
    xdomain.debug = true;
  }
</script>
<![endif]-->
<!-- end xdomain -->
