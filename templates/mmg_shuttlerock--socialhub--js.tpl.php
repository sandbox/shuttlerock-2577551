<?php
/**
 * @file
 * Utilizes JavaScript to create all of the Social Hub content in-page.
 */

?>
<div class="<?= implode(' ', $classes_array) ?>" data-socialhub-name="<?= $socialhubName ?>"></div>
