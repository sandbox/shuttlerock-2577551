<?php

class ShuttlerockBoardEntity extends Entity {
  /**
   * @inherit
   */
  protected function defaultUri() {
    return array('path' => 'shuttlerock/board/' . $this->identifier());
  }
}
