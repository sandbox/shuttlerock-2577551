<?php

class ShuttlerockWidgetEntity extends Entity {
  /**
   * @inherit
   */
  protected function defaultUri() {
    return array('path' => 'shuttlerock/widget/' . $this->identifier());
  }
}
