<?php

class ShuttlerockWidgetEntityUIController extends EntityDefaultUIController {
  /**
   * Generates the render array for a overview table for arbitrary entities
   * matching the given conditions.
   *
   * @param $conditions
   *   An array of conditions as needed by entity_load().

   * @return Array
   *   A renderable array.
   */
  public function overviewTable($conditions = array()) {

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', $this->entityType);

    // Add all conditions to query.
    foreach ($conditions as $key => $value) {
      $query->propertyCondition($key, $value);
    }

    if ($this->overviewPagerLimit) {
      $query->pager($this->overviewPagerLimit);
    }

    $results = $query->execute();

    $ids = isset($results[$this->entityType]) ? array_keys($results[$this->entityType]) : array();
    $entities = $ids ? entity_load($this->entityType, $ids) : array();
    ksort($entities);

    // Load boards
    $board_ids = array();
    foreach ($entities as $entity) {
      $board_ids[$entity->sbid] = $entity->sbid;
    }

    $boards = entity_load('shuttlerock_board', $board_ids);

    $rows = array();
    foreach ($entities as $entity) {
      $extra_cols = array(
        l($boards[$entity->sbid]->board_id, 'admin/shuttlerock/board/manage/' . $entity->sbid),
        $entity->widget_type == 'stream' ? t('Content Stream') : t('Social Hub'),
      );

      // Generate the row and alter as needed
      $row = $this->overviewTableRow($conditions, entity_id($this->entityType, $entity), $entity, $extra_cols);
      $row[0]['data']['#theme'] = 'shuttlerock_ui_widget_overview_item';

      // Add to the listing
      $rows[] = $row;
    }

    $render = array(
      '#theme' => 'table',
      '#header' => $this->overviewTableHeaders($conditions, $rows, array(t('Board'), t('Type'))),
      '#rows' => $rows,
      '#empty' => t('None.'),
    );
    return $render;
  }

  /**
   * Generates the table headers for the overview table.
   */
  protected function overviewTableHeaders($conditions, $rows, $additional_header = array()) {
    $header = $additional_header;
    array_unshift($header, t('Name'));
    if (!empty($this->entityInfo['exportable'])) {
      $header[] = t('State');
    }

    // Add operations with the right colspan.
    $header[] = array('data' => t('Operations'), 'colspan' => $this->operationCount());
    return $header;
  }
}
