<?php
/**
 * @file
 * Controller for the Shuttlerock Widget Entity.
 */

/**
 * Class ShuttlerockWidgetEntityController
 */
class ShuttlerockWidgetEntityController extends EntityAPIControllerExportable {
  public function create(array $values = array()) {
    $values += array(
      'widget_type' => 'hub',
      'locale' => 'en',
      'list_type' => 'board',
    );

    return parent::create($values);
  }

  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $build = parent::buildContent($entity, $view_mode, $langcode, $content);

    $build['#theme'] = 'shuttlerock_widget';
    $build['#entity_type'] = $this->entityType;
    $build['#entity'] = $entity;
    $build['#view_mode'] = $view_mode;
    $build['#language'] = $langcode;

    return $build;
  }
}
